package ru.dremezov.task_1;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

public class FactorialMass {
    private long[] mass;
    private Map<Long, BigInteger> resultMap;

    public FactorialMass() {
        this.mass = generateMass();
        resultMap = new HashMap<>();
    }

    public void start() {
        for (int i = 0; i < mass.length; i++) {
            long key = mass[i];
            BigInteger value = new ForkJoinPool().invoke(new Factorial(mass[i], resultMap));
            System.out.println("Факториал " + key + " равен " + value);
            resultMap.put(key, value);
        }
    }

    /**
     * Функция генерирует случайный массив чисел до 1000 элементов
     */
    private static long[] generateMass() {
        long[] mass = new long[(int) (Math.random() * 1000) + 1];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = (long) (Math.random() * 100);
        }
        return mass;
    }
}

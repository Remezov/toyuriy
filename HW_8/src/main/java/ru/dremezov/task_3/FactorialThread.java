package ru.dremezov.task_3;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

public class FactorialThread implements Runnable{
    private long number;
    private Map<Long, BigInteger> resultMap;

    public FactorialThread(long number, Map<Long, BigInteger> resultMap) {
        this.number = number;
        this.resultMap = resultMap;
    }

    @Override
    public void run() {
        synchronized (resultMap) {
            long key = number;
            BigInteger value = new ForkJoinPool().invoke(new Factorial(number, resultMap));
            System.out.println("Факториал " + key + " равен " + value);
            resultMap.put(key, value);
        }
    }
}

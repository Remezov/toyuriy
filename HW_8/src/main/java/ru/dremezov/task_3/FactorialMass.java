package ru.dremezov.task_3;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FactorialMass {
    private long[] mass;
    private Map<Long, BigInteger> resultMap;

    public FactorialMass() {
        this.mass = generateMass();
        resultMap = new HashMap<>();
    }

    public void start() {
        ExecutorService service = Executors.newFixedThreadPool(20);
        for (int i = 0; i < mass.length; i++) {
            service.submit(new FactorialThread(mass[i], resultMap));
        }
        service.shutdown();
    }

    /**
     * Функция генерирует случайный массив чисел до 1000 элементов
     */
    private static long[] generateMass() {
        long[] mass = new long[(int) (Math.random() * 1000) + 1];
        for (int i = 0; i < mass.length; i++) {
            mass[i] = (long) (Math.random() * 100);
        }
        return mass;
    }
}

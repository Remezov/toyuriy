package ru.dremezov.task_3;

import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.RecursiveTask;

public class Factorial extends RecursiveTask<BigInteger> {
    private long number;
    private Map<Long, BigInteger> resultMap;

    public Factorial(long number, Map<Long, BigInteger> resultMap) {
        this.number = number;
        this.resultMap = resultMap;
    }

    /**
     * Функция использует данные других вычислений.
     */
    @Override
    protected BigInteger compute() {
        BigInteger result = BigInteger.valueOf(number);

        if (resultMap.containsKey(number)) {
            return resultMap.get(number);
        }

        if (number == 0 || number == 1) {
            return BigInteger.valueOf(1);
        }

        Factorial task = new Factorial(number - 1, resultMap);
        task.fork();

        result = result.multiply(task.join());

        return result;
    }
}

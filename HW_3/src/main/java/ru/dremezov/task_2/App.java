package ru.dremezov.task_2;

public class App {
    public static void main(String[] args) throws Exception {

        //генерируем N от 1 до 10
        int n = generator(1, 10);

        int[] mass = new int[n];

        //заполняем массив рандомными значениями от -10 до 10
        for (int i = 0; i < n; i++) {
            mass[i] = generator(-10, 10);
        }

        for (int i = 0; i < n; i++) {
            if (mass[i] >= 0) {
                int a = (int) Math.sqrt(mass[i]);
                a = a * a;
                if (a == mass[i]) {
                    System.out.println(mass[i]);
                }
            } else {
                throw new Exception("В массиве отрицательное число");
            }
        }
    }

    public static int generator(int min, int max) {
        return (int) (Math.random() * (max - min) + min);
    }
}

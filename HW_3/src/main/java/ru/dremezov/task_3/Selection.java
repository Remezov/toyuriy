package ru.dremezov.task_3;

public class Selection implements Sort{
    private Person[] massPerson;

    public Selection(Person[] massPerson) {
        this.massPerson = massPerson;
    }

    @Override
    public Person[] sort() {
        // подсчитываем время сортировки
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < massPerson.length; i++) {
            Person person = massPerson[i];
            int person_i = i;

            for (int j = i + 1; j < massPerson.length; j++) {
                if (person.compareTo(massPerson[j]) > 0) {
                    person = massPerson[j];
                    person_i = j;
                }
            }

            if (i != person_i) {
                swap(i, person_i);
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Время сортировки: " + (endTime - startTime) + " мс");

        return massPerson;
    }

    @Override
    public void print(Person[] massPerson) {
        //Если имена людей и возраст совпадают, выбрасываем пользовательское исключение.
        for (int i = 0; i < massPerson.length; i++) {
            if (i == 0) {
                System.out.println(massPerson[i].toString());
            } else if ((massPerson[i].name == massPerson[i - 1].name) && (massPerson[i].age == massPerson[i - 1].age)) {
                try {
                    throw new Exception("Такой человек уже есть");
                } catch (Exception e) {
                    System.out.println("Такой человек уже есть");;
                }
            } else {
                System.out.println(massPerson[i].toString());
            }
        }
    }

    private void swap(int i, int person_i) {
        Person person;
        person = massPerson[i];
        massPerson[i] = massPerson[person_i];
        massPerson[person_i] = person;
    }
}

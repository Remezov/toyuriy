package ru.dremezov.task_3;

public class Person implements Comparable<Person> {
    int age;
    Sex sex;
    String name;

    //массивы имен для генерации людей
    String[] manName = {"Аркадий", "Борис", "Виктор", "Дмитрий", "Евгений", "Петр", "Сергей", "Юрий", "Павел", "Ахмед"};
    String[] womanName = {"Алиса", "Анна", "Валерия", "Диана", "Лиана", "Ольга", "Екатерина", "Ульяна", "Татьяна", "Светлана"};

    public Person() {
        this.age = (int) (Math.random() * 101);

        int random = (int) (Math.random() * 2);
        if (random == 0) {
            this.sex = Sex.MAN;
        } else {
            this.sex = Sex.WOMAN;
        }

        if (this.sex == Sex.MAN) {
            this.name = manName[(int) (Math.random() * 10)];
        } else {
            this.name = womanName[(int) (Math.random() * 10)];
        }
    }

    @Override
    public int compareTo(Person person) {
        if (this.sex == person.sex) {
            if (this.age == person.age) {
                return this.name.toLowerCase().compareTo(person.name.toLowerCase());
            } else {
                return (person.age - this.age);
            }
        } else {
            return this.sex.ordinal() - person.sex.ordinal();
        }
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", sex=" + sex +
                ", name='" + name + '\'' +
                '}';
    }
}
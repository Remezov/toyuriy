package ru.dremezov.task_3;

public interface Sort {

    Person[] sort();

    void print(Person[] massPerson);
}

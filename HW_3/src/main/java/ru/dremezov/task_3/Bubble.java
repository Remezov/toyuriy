package ru.dremezov.task_3;

public class Bubble implements Sort{
    private Person[] massPerson;

    public Bubble(Person[] massPerson) {
        this.massPerson = massPerson;
    }

    @Override
    public Person[] sort() {
        // подсчитываем время сортировки
        long startTime = System.currentTimeMillis();

        for (int i = (massPerson.length - 1); i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (massPerson[j].compareTo(massPerson[j + 1]) > 0) {
                    swap(j);
                }
            }
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Время сортировки: " + (endTime - startTime) + " мс");

        return massPerson;
    }

    @Override
    public void print(Person[] massPerson) {
        //Если имена людей и возраст совпадают, выбрасываем пользовательское исключение.
        for (int i = 0; i < massPerson.length; i++) {
            if (i == 0) {
                System.out.println(massPerson[i].toString());
            } else if ((massPerson[i].name == massPerson[i - 1].name) && (massPerson[i].age == massPerson[i - 1].age)) {
                try {
                    throw new Exception("Такой человек уже есть");
                } catch (Exception e) {
                    System.out.println("Такой человек уже есть");;
                }
            } else {
                System.out.println(massPerson[i].toString());
            }
        }
    }

    private void swap(int j) {
        Person person;
        person = massPerson[j];
        massPerson[j] = massPerson[j + 1];
        massPerson[j + 1] = person;
    }


}

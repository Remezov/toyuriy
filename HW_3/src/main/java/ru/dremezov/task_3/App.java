package ru.dremezov.task_3;

public class App {
    public static void main(String[] args) {
        //генерируем массив
        int countPerson = 10000;
        Person[] massPerson = new Person[countPerson];
        for (int i = 0; i < countPerson; i++) {
            massPerson[i] = new Person();
        }

        //первая сортировка и вывод массива
        Bubble bubble = new Bubble(massPerson);
        bubble.print(bubble.sort());

        System.out.println();

        //вторая сортировка и вывод массива
        Selection selection = new Selection(massPerson);
        selection.print(selection.sort());
    }
}

package ru.dremezov.task;

import java.sql.SQLException;

public class App {
    public static void main(String[] args) throws SQLException {
        Cabinet cabinet = new Cabinet();
        cabinet.deleteStudent("Petr Petrov");
        cabinet.insertStudent(new Student[]{new Student("Ivan Qwerty", 2), new Student("Qwery Asdfg", 2)});
        cabinet.updateStudentLevel("Ivan Qwerty", 3);
        cabinet.selectStudentsByLevel(2);
        cabinet.closeConnection();
    }
}

package ru.dremezov.task;

import java.sql.*;

public class Cabinet {
    private Connection connection;

    public Cabinet() throws SQLException {
        this.connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/postgres",
                "postgres",
                "postgres");
        DBUtil.renewDatabase();
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    public void selectStudentsByLevel(int level) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT * FROM student WHERE level = ?")) {
            preparedStatement.setInt(1, level);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    System.out.print("id=" + resultSet.getInt("id"));
                    System.out.print("; name=" + resultSet.getString("name"));
                    System.out.print("; level=" + resultSet.getInt("level") + "\n");
                }
            }
        }
    }

    public void insertStudent(Student[] student) throws SQLException {
        connection.setAutoCommit(false);
        Savepoint savepoint = connection.setSavepoint();
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO student (name, level) VALUES (?, ?)")) {
            for (int i = 0; i < student.length; i++) {
                preparedStatement.setString(1, student[i].getName());
                preparedStatement.setInt(2, student[i].getLevel());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        }

        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT * FROM student")) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    if (resultSet.getString("name").split(" ").length != 2) {
                        connection.rollback(savepoint);
                    }
                }
            }
        }
        connection.commit();
    }

    public void updateStudentLevel(String name, int level) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);
            statement.executeUpdate("UPDATE student SET level = " + level + " WHERE name = '" + name + "'");
            connection.commit();
        }
    }

    public void deleteStudent(String name) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "DELETE FROM student WHERE name = ?")) {
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
        }
    }
}

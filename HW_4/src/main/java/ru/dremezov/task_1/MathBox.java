package ru.dremezov.task_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class MathBox extends ObjectBox<Number>{

    public MathBox(Number[] mass) {
        super();
        //проверяем массив на уникальность элементов
        for (int i = 0; i < mass.length; i++) {
            Number number = mass[i];
            for (int j = i + 1; j < mass.length; j++) {
                if (number.equals(mass[j])) {
                    throw new IllegalArgumentException("В массиве есть дубли!");
                }
            }
        }
        super.objects = new ArrayList<>(Arrays.asList(mass));
    }

    public double summator() {
        double sum = 0;
        for (Number number : super.objects) {
            sum += number.doubleValue();
        }
        return sum;
    }

    public List<Number> splitter(double divider) {
        for (int i = 0; i < super.objects.size(); i++) {
            super.objects.set(i, super.objects.get(i).doubleValue() / divider);
        }
        return super.objects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MathBox mathBox = (MathBox) o;
        return this.objects.equals(mathBox.objects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(objects);
    }

    @Override
    public String toString() {
        return "MathBox{" +
                "numbers=" + objects +
                '}';
    }

    public List<Number> findRemove(Integer verifiable) {
        if (super.objects.contains(verifiable)) {
            super.objects.remove(verifiable);
        }
        return super.objects;
    }
}

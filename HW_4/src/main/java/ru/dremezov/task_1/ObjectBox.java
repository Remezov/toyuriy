package ru.dremezov.task_1;

import java.util.List;

public class ObjectBox<T> {
    protected List<T> objects;

    public ObjectBox(List<T> objects) {
        this.objects = objects;
    }

    public ObjectBox() {

    }

    public void addObject(T subject) {
        this.objects.add(subject);
    }

    public void deleteObject(T verifiable) {
        if (this.objects.contains(verifiable)) {
            this.objects.remove(verifiable);
        }
    }

    public void dump() {
        System.out.print("Коллекция: ");
        for (int i = 0; i < this.objects.size(); i++) {
            if (i == this.objects.size() - 1) {
                System.out.print(this.objects.get(i).toString() + ".");
                return;
            }
            System.out.print(this.objects.get(i).toString() + "; ");
        }
    }
}

package ru.dremezov.task_1;

import java.util.List;

public class App {
    public static void main(String[] args) {
        Number[] mass = {1, 2, 2.3, 3};
        MathBox mathBox = new MathBox(mass);

        mathBox.addObject(1);

//        double sum = mathBox.summator();
//        System.out.println(sum);

        List<Number> splitter = mathBox.splitter(3);
        System.out.println(splitter);

        List<Number> checkInt = mathBox.findRemove(3);
        System.out.println(checkInt);

    }
}

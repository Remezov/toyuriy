package ru.dremezov.task_1.server;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    public static List<ServerThread> serverList = new LinkedList<>();
    public static Map<String,ServerThread> nameMap = new ConcurrentHashMap<>();

    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(45544, 0, InetAddress.getByName("127.0.0.1"));
        System.out.println("Сервер запущен");
        while (true) {
            Socket socket = server.accept();
            ServerThread serverThread = new ServerThread(socket);
            serverList.add(serverThread);
            serverThread.start();
        }
    }
}

package ru.dremezov.task_1.server;

import java.io.*;
import java.net.Socket;

import static ru.dremezov.task_1.server.Server.*;

public class ServerThread extends Thread {
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;

    public ServerThread(Socket socket) throws IOException {
        this.socket = socket;
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
    }

    @Override
    public void run() {
        String str;
        try {
            while (true) {
                str = reader.readLine();
                if (nameMap.containsKey(str)) {
                    send("Это имя занято");
                    continue;
                }
                nameMap.put(str, this);
                send("Привет!");
                break;
            }

            try {
                while (true) {
                    str = reader.readLine();

                    if (str.equals("quit")) {
                        this.downSocket();
                        break;
                    }

                    if (str.charAt(0) == '@') {
                        String[] mass = str.split(" ");
                        String name = mass[0].substring(1);
                        nameMap.get(name).send(str);
                        continue;
                    }

                    System.out.println("Echo: " + str);
                    for (ServerThread st : serverList) {
                        st.send(str);
                    }
                }
            } catch (NullPointerException e) {
                System.out.println("Словил NPE");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void send(String str) {
        try {
            writer.write(str + "\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void downSocket() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                reader.close();
                writer.close();
                for (ServerThread st : serverList) {
                    if (st.equals(this)) {
                        st.interrupt();
                    }
                    serverList.remove(this);
                }
            }
        } catch (IOException e) {
            System.out.println("Ошибка при остановке сокета");
        }
    }
}

package ru.dremezov.task_1.client;

import java.io.*;
import java.net.Socket;

public class ClientThread {
    private Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private BufferedReader inputUser;
    private String name;
    private boolean controller = false;

    public ClientThread() throws IOException {
        this.socket = new Socket("127.0.0.1", 45544);
        inputUser = new BufferedReader(new InputStreamReader(System.in));
        reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        this.enterName();
        new ReadThread().start();
        new WriteThread().start();

    }

    private void enterName() throws IOException {
        System.out.print("Ваше имя: ");
        name = inputUser.readLine();

        writeMsg(name + "\n");
    }

    private void quit() {
        try {
            if (!socket.isClosed()) {
                socket.close();
                reader.close();
                writer.close();
            }
        } catch (IOException e) {
            System.out.println("Ошибка при остановке клиента");
        }
    }

    private void writeMsg(String msg) throws IOException {
        writer.write(msg);
        writer.flush();
    }

    private class ReadThread extends Thread {
        @Override
        public void run() {
            String str;
            try {
                while (true) {
                    str = reader.readLine();

                    if (str.equals("quit")) {
                        ClientThread.this.quit();
                        break;
                    }

                    if (str.equals("Это имя занято")) {
                        System.out.println(str);
                        enterName();
                        continue;
                    }

                    if (str.equals("Привет!")) {
                        controller = true;
                    }

                    System.out.println(str);
                }
            } catch (IOException e) {
                System.out.println("Ошибка чтения-записи нити чтения");
            }
        }
    }

    public class WriteThread extends Thread {
        @Override
        public void run() {
            while (true) {
                String str;
                try {
                    if(!controller) {
                        Thread.sleep(1000);
                        continue;
                    }
                    str = inputUser.readLine();

                    if (str.equals("quit")) {
                        writeMsg("quit");
                        ClientThread.this.quit();
                        break;
                    }

                    if (str.charAt(0) == '@') {
                        String[] mass = str.split(" ");
                        writeMsg(str + " | from " + name + "\n");
                        continue;
                    }

                    writeMsg(name + ": " + str + "\n");
                } catch (IOException | InterruptedException e) {
                    System.out.println("Ошибка чтения-записи нити записи");
                }
            }
        }
    }
}
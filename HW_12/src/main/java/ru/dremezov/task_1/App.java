package ru.dremezov.task_1;

public class App {
    public static void main(String[] args) {
        FileCabinet fileCabinet = new FileCabinet();
        fileCabinet.addPet(new Pet("Sharik", new Person("Vasya", 18, Sex.MAN), 4));
        fileCabinet.addPet(new Pet("Bobik", new Person("Liana", 19, Sex.WOMAN), 10.6));
        fileCabinet.addPet(new Pet("Kuzya", new Person("Alla", 20, Sex.WOMAN), 18));
        fileCabinet.addPet(new Pet("Tuzik", new Person("Alla", 18, Sex.WOMAN), 5.3));
        fileCabinet.addPet(new Pet("Tuzik", new Person("Vasya", 20, Sex.MAN), 5.3));

        fileCabinet.sortPets();
    }
}

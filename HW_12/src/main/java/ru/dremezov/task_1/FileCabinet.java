package ru.dremezov.task_1;

import java.util.*;
import java.util.stream.Collectors;

public class FileCabinet {
    private Map<Integer, Pet> petMap;
    private Map<Integer, List<Integer>> nicknameMap;

    public FileCabinet() {
        this.petMap = new HashMap<>();
        this.nicknameMap = new HashMap<>();
    }

    /**
     * Mетод добавления животного в общий список (учесть, что добавление дубликатов должно приводить к исключительной ситуации)
     */

    public void addPet(Pet pet) {
        if (!petMap.containsKey(pet.getId())) {
            petMap.put(pet.getId(), pet);
            addObjectAtNicknameMap(pet.getId(), pet.getNickname());
        } else {
            System.out.println("Дублирующий id. " + pet.toString());
        }
    }

    /**
     * Поиск животного по его кличке (поиск должен быть эффективным)
     */

    public void findPetByNickname(String nickname) {
        try {
            for (Integer key : nicknameMap.get(nickname.hashCode())) {
                System.out.println(petMap.get(key));
            }
        } catch (NullPointerException e) {
            System.out.println("Нет данных");
        }
    }

    /**
     * изменение данных животного по его идентификатору
     */

    public void changeNickname(Integer id, String newNickname) {
        String nicknameBefore = petMap.get(id).getNickname();

        //меняем кличку животного в petMap
        petMap.get(id).setNickname(newNickname);

        //обновляем данные в nicknameMap
        nicknameMap.get(nicknameBefore.hashCode()).remove(id);
        addObjectAtNicknameMap(id, newNickname);
    }

    public void changeOwner(Integer id, Person owner) {
        petMap.get(id).setOwner(owner);
    }

    public void changeWeight(Integer id, double weight) {
        petMap.get(id).setWeight(weight);
    }

    /**
     * Вывод на экран списка животных в отсортированном порядке. Поля для сортировки –  хозяин, кличка животного, вес.
     */
    public void sortPets() {
        Comparator<Pet> petComparator = (pet1, pet2) -> {
            int result = pet1.getOwner().getName().compareTo(pet2.getOwner().getName());
            if (result != 0) {
                return result;
            }
            result = pet1.getNickname().compareTo(pet2.getNickname());
            if (result != 0) {
                return result;
            }
            return  (int) (pet1.getWeight() - pet2.getWeight());
        };

        List<Pet> sortedPetList =
                new ArrayList<>(petMap.values()).stream()
                        .sorted(petComparator)
                        .collect(Collectors.toList());

        for (Pet p : sortedPetList) {
            System.out.println(p.toString());
        }
    }

    private void addObjectAtNicknameMap(Integer id, String newNickname) {
        nicknameMap.putIfAbsent(newNickname.hashCode(), new ArrayList<>());
        nicknameMap.get(newNickname.hashCode()).add(id);
    }
}

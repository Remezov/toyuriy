package ru.dremezov.task;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil {

    public DBUtil() {
    }

    public static void renewDatabase(Connection connection) throws SQLException {
        try (Statement statement = connection.createStatement();) {
            statement.execute("-- Database: postgres\n"
                    + "DROP TABLE IF EXISTS student;"
                    + "\n"
                    + "CREATE TABLE student (\n"
                    + "    id bigserial primary key,\n"
                    + "    name varchar(100) NOT NULL,\n"
                    + "    level integer NOT NULL);"
                    + "\n"
                    + "INSERT INTO student (name, level)\n"
                    + "VALUES\n"
                    + "   ('Vasilyj Rogov', 3),\n"
                    + "   ('Petr Petrov', 2),\n"
                    + "   ('Ivan Ivaniv', 2),\n"
                    + "   ('Mihail Mihajlob', 1);"
                    + "\n"
                    + "DROP TABLE IF EXISTS teacher;"
                    + "\n"
                    + "CREATE TABLE teacher (\n"
                    + "    id bigserial primary key,\n"
                    + "    name varchar(100) NOT NULL);"
                    + "\n"
                    + "INSERT INTO teacher (name)\n"
                    + "VALUES\n"
                    + "   ('Sergej Sergeev'),\n"
                    + "   ('Irina Irinova');"
                    + "\n"
                    + "DROP TABLE IF EXISTS test;"
                    + "\n"
                    + "CREATE TABLE test (\n"
                    + "    id bigserial primary key,\n"
                    + "    level integer NOT NULL);"
                    + "\n"
                    + "INSERT INTO test (level )\n"
                    + "VALUES\n"
                    + "   (1),\n"
                    + "   (2),\n"
                    + "   (3),\n"
                    + "   (4);"
                    + "\n");
        }
    }
}

package ru.dremezov.task;

public class App {
    public static void main(String[] args) {
        try (Cabinet cabinet = new Cabinet()) {
            cabinet.deleteStudent("Petr Petrov");
            cabinet.insertStudent(new Student[]{new Student("Ivan", 2), new Student("Qwery Asdfg", 2)});
            cabinet.updateStudentLevel("Ivan Qwerty", 3);
            cabinet.selectStudentsByLevel(2);
        }
    }
}

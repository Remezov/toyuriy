package ru.dremezov.task_1;

public class Pet {
    private Animal animal;

    public Pet(Animal animal) {
        this.animal = animal;
    }

    public Animal getAnimal() {
        return animal;
    }

    public int getId() {
        return animal.getId();
    }

    public String getNickname() {
        return animal.getNickname();
    }

    public void setNickname(String nickname) {
        animal.setNickname(nickname);
    }

    public Person getOwner() {
        return animal.getOwner();
    }

    public void setOwner(Person owner) {
        animal.setOwner(owner);
    }

    public double getWeight() {
        return animal.getWeight();
    }

    public void setWeight(double weight) {
        animal.setWeight(weight);
    }

    public String toString() {
        return animal.toString();
    }
}

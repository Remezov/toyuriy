package ru.dremezov.task_1;

import java.util.Map;

public interface FileCabinetInterface {
    SpecyIterator createBirdIterator();
    SpecyIterator createFishIterator();
    SpecyIterator createMammalIterator();
    SpecyIterator createReptileIterator();
}

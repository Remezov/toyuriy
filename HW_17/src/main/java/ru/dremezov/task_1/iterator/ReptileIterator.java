package ru.dremezov.task_1.iterator;

import ru.dremezov.task_1.Pet;
import ru.dremezov.task_1.SpecyIterator;
import ru.dremezov.task_1.specy.Reptile;

import java.util.Map;

public class ReptileIterator implements SpecyIterator {
    private Map<Integer, Pet> petMap;

    public ReptileIterator(Map<Integer, Pet> petMap) {
        this.petMap = petMap;
    }

    public void print() {
        for (Pet p : petMap.values()) {
            if (p.getAnimal() instanceof Reptile) {
                System.out.println(p.toString());
            }
        }
    }
}

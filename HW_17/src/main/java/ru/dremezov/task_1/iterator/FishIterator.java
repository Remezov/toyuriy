package ru.dremezov.task_1.iterator;

import ru.dremezov.task_1.Pet;
import ru.dremezov.task_1.SpecyIterator;
import ru.dremezov.task_1.specy.Fish;

import java.util.Map;

public class FishIterator implements SpecyIterator {
    private Map<Integer, Pet> petMap;

    public FishIterator(Map<Integer, Pet> petMap) {
        this.petMap = petMap;
    }

    public void print() {
        for (Pet p : petMap.values()) {
            if (p.getAnimal() instanceof Fish) {
                System.out.println(p.toString());
            }
        }
    }
}

package ru.dremezov.task_1;

import ru.dremezov.task_1.specy.Bird;
import ru.dremezov.task_1.specy.Fish;
import ru.dremezov.task_1.specy.Mammal;
import ru.dremezov.task_1.specy.Reptile;

public class App {
    public static void main(String[] args) {
        FileCabinet fileCabinet = new FileCabinet();
        fileCabinet.addPet(new Pet(new Fish("Sharik", new Person("Vasya", 18, Sex.MAN), 4)));
        fileCabinet.addPet(new Pet(new Mammal("Bobik", new Person("Liana", 19, Sex.WOMAN), 10.6)));
        fileCabinet.addPet(new Pet(new Bird("Kuzya", new Person("Alla", 20, Sex.WOMAN), 18)));
        fileCabinet.addPet(new Pet(new Reptile("Tuzik", new Person("Alla", 18, Sex.WOMAN), 5.3)));
        fileCabinet.addPet(new Pet(new Reptile("Tuzik", new Person("Vasya", 20, Sex.MAN), 5.3)));
        fileCabinet.addPet(new Pet(new Bird("Tuzik", new Person("Ira", 20, Sex.MAN), 5.3)));

        fileCabinet.createMammalIterator().print();
    }
}

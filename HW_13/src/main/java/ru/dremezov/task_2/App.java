package ru.dremezov.task_2;

import javassist.ClassPool;

//-XX:MaxMetaspaceSize=10m  -XX:MetaspaceSize=2M -XX:MaxMetaspaceFreeRatio=1 -XX:MaxMetaspaceExpansion=1K -XX:MinMetaspaceFreeRatio=1 -XX:InitialBootClassLoaderMetaspaceSize=2M

public class App {
        static ClassPool classPool = ClassPool.getDefault();

        public static void main(String[] args) throws Exception {
            for (int i = 0; i < 1000000; i++) {
                Class c = classPool.makeClass(
                        i + " outofmemory.OutOfMemoryErrorMetaspace ").toClass();
            }
        }

}


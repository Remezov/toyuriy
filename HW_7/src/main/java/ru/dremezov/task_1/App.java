package ru.dremezov.task_1;

import java.io.*;
import java.util.*;

public class App {
    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("src/main/resources/text.txt"));
            String line = null;
            List<String> wordList = new ArrayList<>();
            while ((line = br.readLine()) != null) {
                wordList.addAll(Arrays.asList(line.split(" ")));
            }

            Set<String> wordSet = new TreeSet<>();
            for (String word : wordList) {
                wordSet.add(word.toLowerCase());
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter("src/main/resources/result.txt"));
            for (String word : wordSet) {
                bw.write(word.toLowerCase() + " ");
                bw.flush();
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (IOException e) {
            System.out.println("File read error.");
        }
    }
}

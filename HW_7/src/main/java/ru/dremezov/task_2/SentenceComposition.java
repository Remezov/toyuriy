package ru.dremezov.task_2;

public enum SentenceComposition {
    START,
    MIDDLE,
    END,
    ONE_WORD
}

package ru.dremezov.task_2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static ru.dremezov.task_2.SentenceComposition.*;

public class App {
    public static void main(String[] args) {
        getFiles("src/main/resources/new_folder", 3, 10000, generateWordArray(), 25);
    }

    /**
     * ������� ���������� ����� �� 1<=n2<=15 ��������� ����, � ����������� �� ������� � �����������. �����������
     * �������� ���� ����� � �����������. ����� ���������� �������.
     */
    private static String generateWord(SentenceComposition position) {
        int n2 = generateNumber(15, 1);
        String word = "";
        for (int i = 1; i <= n2; i++) {
            if ((position == START || position == ONE_WORD) && i == 1) {
                word = (word + (char) generateNumber(26, 97)).toUpperCase();
                continue;
            }

            word += (char) generateNumber(26, 97);
        }

        if (position != END && position != ONE_WORD) {
            String comma = "";
            switch (generateNumber(2, 1)) {
                case 1:
                    comma = ", ";
                    break;
                case 2:
                    comma = " ";
            }

            word += comma;
        }
        return word;
    }

    /**
     * ������� ���������� ����������� �� 1<=n1<=15 ����. ���������� ������� ����� � �����������. � ����������� ��
     * probability ��������� ����� �� ������� � �������� �����������.
     * ���� ������ ���� 1<=n4<=1000. ���� ����������� probability ��������� ������ �� ���� ����� ������� � ���������
     * ����������� (1/probability).
     */
    private static String generateSentence(String[] massWord, int probability) {
        int n1 = generateNumber(15, 1);
        String sentence = "";
        for (int i = 1; i <= n1; i++) {
            if (i == 1) {
                if (i == n1) {
                    sentence += generateWord(ONE_WORD) + generateLastCharacter();
                    continue;
                }
                sentence += generateWord(START);
                continue;
            }

            if (i == n1) {
                sentence += generateWord(END) + generateLastCharacter() + " ";
                continue;
            }

            if (Math.random() <= 1 / probability) {
                sentence += massWord[generateNumber(1000,1)];
            }
            sentence += generateWord(MIDDLE);
        }
        return sentence;
    }

    /**
     * ������� ���������� ����� �� 1<=n3<=20 �����������
     */
    private static String generateParagraph(String[] massWord, int probability) {
        int n3 = generateNumber(20, 1);
        String paragraph = "";
        for (int i = 0; i <= n3; i++) {
            paragraph += generateSentence(massWord, probability);
        }
        return paragraph + "\r\n";
    }

    private static void getFiles(String path, int n, int size, String[] massWord, int probability) {
        new File(path).mkdir();
        for (int i = 1; i <= n; i++) {

            try (FileWriter writer = new FileWriter(path + "/file" + i + ".txt")){
                int plannedFileSize = size;
                String generatedText;
                do {
                    generatedText = generateParagraph(massWord, probability);
                    plannedFileSize -= generatedText.getBytes(StandardCharsets.UTF_8).length;
                    writer.write(generatedText);
                    writer.flush();
                } while (plannedFileSize > 0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * ������� ���������� ������ �� 1<=n4<=1000 ����
     */
    private static String[] generateWordArray() {
        int n4 = generateNumber(1000, 1);
        String[] mass = new String[n4];
        for (int i = 0; i < n4; i++) {
            mass[i] = generateWord(MIDDLE);
        }
        return mass;
    }

    private static int generateNumber(int count, int offset) {
        return (int) (Math.random() * count) + offset;
    }

    private static String generateLastCharacter() {
        String lastCharacter = "";
        switch (generateNumber(3, 1)) {
            case 1:
                lastCharacter = ".";
                break;
            case 2:
                lastCharacter = "!";
                break;
            case 3:
                lastCharacter = "?";
                break;
        }
        return lastCharacter;
    }
}

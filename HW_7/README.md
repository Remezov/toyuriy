������� 1. �������� ���������, �������� ��������� ����. ��������� ������ ���������� ��������������� �� �������� ������
����, ��������� � ����� � ��������� ��� � ����-���������. ��������� ����� �� ������ �����������, ������� �� ������ ���-
��������. ���� ����� � ������ ������� � ��� ������ �����.


������� 2. ������� ��������� ��������� ������, ���������� �� ��������� ��������:

- ����������� ������� �� 1<=n1<=15 ����. � ����������� ����� ������������ ���� ����� ���������� �������.
- ����� ������� �� 1<=n2<=15 ��������� ����
- ����� ��������� ����� ��������
- ����������� ���������� � ��������� �����
- ����������� ������������� (.|!|?)+" "
- ����� ������� �� �������. � ����� ������ 1<=n3<=20 �����������. � ����� ������ ����� ������ ������ � ������� �������.
- ���� ������ ���� 1<=n4<=1000. ���� ����������� probability ��������� ������ �� ���� ����� ������� � ��������� ������-
����� (1/probability).

���������� �������� ����� getFiles(String path, int n, int size, String[] words, int probability), ������� ������� n
������ �������� size � �������� path. words - ������ ����, probability - �����������.
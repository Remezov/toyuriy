package ru.dremezov.task_1;

import ru.dremezov.task_1.specy.Bird;
import ru.dremezov.task_1.specy.Fish;
import ru.dremezov.task_1.specy.Mammal;
import ru.dremezov.task_1.specy.Reptile;

public class PetFactory {

    public Pet create(AminalSpecy specy, String nickname, Person owner, double weight) {
        Pet pet = null;
        switch (specy) {
            case BIRD:
                pet = new Bird(nickname, owner, weight);
                break;
            case FISH:
                pet = new Fish(nickname, owner, weight);
                break;
            case MAMMAL:
                pet = new Mammal(nickname, owner, weight);
                break;
            case REPTILE:
                pet = new Reptile(nickname, owner, weight);
                break;
        }
        return pet;
    }
}

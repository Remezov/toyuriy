package ru.dremezov.task_1;

import static ru.dremezov.task_1.AminalSpecy.*;

public class App {
    public static void main(String[] args) {
        FileCabinet fileCabinet = new FileCabinet();
        fileCabinet.addPet(FISH, "Sharik", new Person("Vasya", 18, Sex.MAN), 4);
        fileCabinet.addPet(MAMMAL, "Bobik", new Person("Liana", 19, Sex.WOMAN), 10.6);
        fileCabinet.addPet(BIRD, "Kuzya", new Person("Alla", 20, Sex.WOMAN), 18);
        fileCabinet.addPet(REPTILE, "Tuzik", new Person("Alla", 18, Sex.WOMAN), 5.3);
        fileCabinet.addPet(REPTILE, "Tuzik", new Person("Vasya", 20, Sex.MAN), 5.3);

        fileCabinet.sortPets();
    }
}

package ru.dremezov.task_1;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException{
        Scanner scanner = new Scanner(System.in);
        String str;
        String code = "";
        while (!(str = scanner.nextLine()).isEmpty()) {
            code += str + "\r\n";
        }

        File file = new File("./SomeClass.java");
        try (RandomAccessFile ra = new RandomAccessFile(file, "rw")) {
            ra.skipBytes(102);
            ra.writeBytes(  code + "}}");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        compiler.run(null, null, null, file.getAbsolutePath());

        ClassLoader loader = new MyClassLoader(new String[] {"."});
        try {
            Class cl = Class.forName("SomeClass", true, loader);
            Worker someClass = (Worker) cl.newInstance();
            someClass.doWork();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}

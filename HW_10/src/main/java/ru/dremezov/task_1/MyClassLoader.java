package ru.dremezov.task_1;

import java.io.*;
import java.util.Map;

public class MyClassLoader extends ClassLoader{

    private Map classesHash = new java.util.HashMap();
    public final String[] classPath;

    public MyClassLoader(String[] classPath) {
        this.classPath = classPath;
    }

    @Override
    protected synchronized Class loadClass(String name,
                                           boolean resolve)
            throws ClassNotFoundException
    {
        Class result= findClass(name);
        if (resolve)
            resolveClass(result);
        return result;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        Class result= (Class)classesHash.get(name);
        if (result!=null) {
            System.out.println("% Class " + name + " found in cache");
            return result;
        }

        File file = findFile(name.replace('.','/'),".class");

        if (file==null) {
            return findSystemClass(name);
        }

        try {
            byte[] classBytes= loadFileAsBytes(file);
            result= defineClass(name, classBytes, 0,
                    classBytes.length);
        } catch (IOException e) {
            throw new ClassNotFoundException(
                    "Cannot load class " + name + ": " + e);
        } catch (ClassFormatError e) {
            throw new ClassNotFoundException(
                    "Format of class file incorrect for class "
                            + name + " : " + e);
        }
        classesHash.put(name,result);
        return result;
    }

    @Override
    protected java.net.URL findResource(String name)
    {
        File f= findFile(name, "");
        if (f==null)
            return null;
        try {
            return f.toURL();
        } catch(java.net.MalformedURLException e) {
            return null;
        }
    }

    private File findFile(String name, String extension)
    {
        File file;
        for (int k=0; k <classPath.length; k++) {
            file = new File((new File(classPath[k])).getPath()
                    + File.separatorChar
                    + name.replace('/',
                    File.separatorChar)
                    + extension);
            if (file.exists())
                return file;
        }
        return null;
    }

    public static byte[] loadFileAsBytes(File file)
            throws IOException
    {
        byte[] result = new byte[(int)file.length()];
        FileInputStream f = new FileInputStream(file);
        try {
            f.read(result,0,result.length);
        } finally {
            try {
                f.close();
            } catch (Exception e) {
            };
        }
        return result;
    }
}

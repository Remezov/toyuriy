package ru.dremezov.task;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TestForDBUtil {
    @Mock
    Connection connection;
    @Mock
    Statement statement;

    @BeforeEach
    void setUp() throws SQLException {
        initMocks(this);
        when(connection.createStatement()).thenReturn(statement);
    }

    @Test
    void testNewDBUtil() {
        Assertions.assertDoesNotThrow(() -> new DBUtil());
    }

    @Test
    void testRenewDatabase() {
        Assertions.assertDoesNotThrow(() -> DBUtil.renewDatabase(connection));
    }
}

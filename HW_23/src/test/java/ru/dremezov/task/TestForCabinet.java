package ru.dremezov.task;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.dremezov.task.mocks.ConnectionMock;

public class TestForCabinet {
    static Cabinet cabinet;

    @BeforeAll
    static void setUp() {
        cabinet = new Cabinet(new ConnectionMock());
    }

    @Test
    void testNewCabinet() {
        Assertions.assertDoesNotThrow(() -> new Cabinet(new ConnectionMock()));
    }

    @Test
    void testSelectStudentsByLevel() {
        Assertions.assertDoesNotThrow(() -> cabinet.selectStudentsByLevel(2));
    }

    @Test
    void testInsertStudent() {
        Assertions.assertDoesNotThrow(() -> cabinet
                .insertStudent(new Student[]{new Student("Ivan Ivanov", 2), new Student("Qwery Asdfg", 2)}));
    }

    @Test
    void testUpdateStudentLevel() {
        Assertions.assertDoesNotThrow(() -> cabinet.updateStudentLevel("Ivan Qwerty", 3));
    }

    @Test
    void testDeleteStudent() {
        Assertions.assertDoesNotThrow(() -> cabinet.deleteStudent("Ivan Qwerty"));
    }

    @Test
    void testClose() {
        Assertions.assertDoesNotThrow(() -> cabinet.close());
    }
}

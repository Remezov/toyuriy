package ru.dremezov.task;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestForStudent {
    String name = "Dmitry Remezov";
    int level = 2;
    Student student = new Student(name, level);

    @Test
    void testNewStudent() {
        Assertions.assertEquals(student.getName(), name);
        Assertions.assertEquals(student.getLevel(), level);
    }
}

package ru.dremezov.task;

public class Student {
    private String name;
    private int level;

    public Student(String name, int level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }
}

package ru.dremezov.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.sql.*;

public class Cabinet implements Closeable{
    private Connection connection;
    private static final Logger logger = LogManager.getLogger();
    private static final Logger loggerSecurity = LogManager.getLogger("security");
    private static final Logger loggerBusiness = LogManager.getLogger("business");

    public Cabinet(Connection connection) {
        logger.info("Cabinet started.");
        this.connection = connection;
        logger.info("DB connected.");
        loggerSecurity.info("User \"postgres\" connected DB \"jdbc:postgresql://localhost:5432/postgres\"");

        try {
            DBUtil.renewDatabase(connection);
            logger.info("DB updated.");
        } catch (SQLException throwables) {
            logger.error("DB init or update error.", throwables);
        }
    }

    public void selectStudentsByLevel(int level) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT * FROM student WHERE level = ?")) {
            preparedStatement.setInt(1, level);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    System.out.print("id=" + resultSet.getInt("id"));
                    System.out.print("; name=" + resultSet.getString("name"));
                    System.out.print("; level=" + resultSet.getInt("level") + "\n");
                }
            }
        } catch (SQLException throwables) {
            logger.error("selectStudentsByLevel error.", throwables);
        }
        loggerBusiness.info("Select Students By Level = " + level);
    }

    public void insertStudent(Student[] student) {
        int controller = 0;
        try {
            connection.setAutoCommit(false);
            Savepoint savepoint = connection.setSavepoint();
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO student (name, level) VALUES (?, ?)")) {
                for (int i = 0; i < student.length; i++) {
                    preparedStatement.setString(1, student[i].getName());
                    preparedStatement.setInt(2, student[i].getLevel());
                    preparedStatement.addBatch();
                }
                preparedStatement.executeBatch();
            }
            try (PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM student")) {
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        if (resultSet.getString("name").split(" ").length != 2) {
                            connection.rollback(savepoint);
                            logger.error("Trying to add a student with a name " + resultSet.getString("name"));
                            controller = 1;
                        }
                    }
                }
            }
            connection.commit();
        } catch (SQLException throwables) {
            logger.error("insertStudent error.", throwables);
        }
        if (controller == 0) {
            for (int i = 0; i < student.length; i++) {
                loggerBusiness.info("Insert Student " + student[i].getName());
            }
        }
    }

    public void updateStudentLevel(String name, int level) {
        try (Statement statement = connection.createStatement()) {
            connection.setAutoCommit(false);
            statement.executeUpdate("UPDATE student SET level = " + level + " WHERE name = '" + name + "'");
            connection.commit();
        } catch (SQLException throwables) {
            logger.error("updateStudentLevel error.", throwables);
        }
        loggerBusiness.info("Update Student " + name+ "Level By " + level);
    }

    public void deleteStudent(String name) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "DELETE FROM student WHERE name = ?")) {
            preparedStatement.setString(1, name);
            preparedStatement.executeUpdate();
        } catch (SQLException throwables) {
            logger.error("deleteStudent error.", throwables);
        }
        loggerBusiness.info("Delete Student " + name);
    }

    @Override
    public void close() {
        try {
            connection.close();
            logger.info("Cabinet stopped.");
        } catch (SQLException throwables) {
            logger.error("Connection stop error.", throwables);
        }
    }
}

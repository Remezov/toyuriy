package ru.dremezov.task_1;

import java.util.Objects;

public class Pet implements Comparable<Pet>{
    private final int id;
    private String nickname;
    private Person owner;
    private double weight;

    public Pet(String nickname, Person owner, double weight) {
        this.id = Objects.hash(nickname, owner, weight);
        this.nickname = nickname;
        this.owner = owner;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public Person getOwner() {
        return owner;
    }

    public double getWeight() {
        return weight;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", owner=" + owner +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return id == pet.id && Double.compare(pet.weight, weight) == 0 && Objects.equals(nickname, pet.nickname) && Objects.equals(owner, pet.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nickname, owner, weight);
    }

    @Override
    public int compareTo(Pet pet) {
        if (this.owner.compareTo(pet.owner) == 0) {
            if (this.nickname.toLowerCase().compareTo(pet.nickname.toLowerCase()) == 0) {
                return (int) (this.weight - pet.weight);
            } else {
                return this.nickname.toLowerCase().compareTo(pet.nickname.toLowerCase());
            }
        } else {
            return this.owner.compareTo(pet.owner);
        }
    }

}

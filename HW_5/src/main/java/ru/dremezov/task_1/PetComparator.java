package ru.dremezov.task_1;

import java.util.Comparator;

public class PetComparator implements Comparator<Pet> {
    @Override
    public int compare(Pet o1, Pet o2) {
        int result = o1.getOwner().compareTo(o2.getOwner());
        if (result != 0) {
            return result;
        }
        result = o1.getNickname().toLowerCase().compareTo(o2.getNickname().toLowerCase());
        if (result != 0) {
            return result;
        }
        return  (int) (o1.getWeight() - o2.getWeight());
    }
}

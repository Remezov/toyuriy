package ru.dremezov.task_1;

public class App {
    public static void main(String[] args) {
        FileCabinet fileCabinet = new FileCabinet();
        fileCabinet.addPet(new Pet("Тузик", new Person("Вася", 18, Sex.MAN), 4));
        fileCabinet.addPet(new Pet("Бобик", new Person("Лиана", 19, Sex.WOMAN), 10.6));
        fileCabinet.addPet(new Pet("Кузя", new Person("Аделина", 20, Sex.WOMAN), 18));
        fileCabinet.addPet(new Pet("Кузя", new Person("Аделина", 20, Sex.WOMAN), 5.3));
        fileCabinet.addPet(new Pet("Вася", new Person("Вася", 20, Sex.MAN), 5.3));

//        fileCabinet.changeNickname(145643690, "Кузя");
//
//        fileCabinet.findPetByNickname("Кузя");
//        fileCabinet.findPetByNickname("Бобик");
        fileCabinet.sortPets();

    }
}

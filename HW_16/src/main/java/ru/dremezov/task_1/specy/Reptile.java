package ru.dremezov.task_1.specy;

import ru.dremezov.task_1.Person;
import ru.dremezov.task_1.Animal;

import java.util.Objects;

public class Reptile implements Animal {
    private final int id;
    private String nickname;
    private Person owner;
    private double weight;

    public Reptile(String nickname, Person owner, double weight) {
        this.id = Objects.hash(nickname, owner, weight);
        this.nickname = nickname;
        this.owner = owner;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Reptile{" +
                "id=" + id +
                ", nickname='" + nickname + '\'' +
                ", owner=" + owner +
                ", weight=" + weight +
                '}';
    }

    @Override
    public void vaccination() {
        System.out.println("Поставлен укол для рептилий.");
    }
}

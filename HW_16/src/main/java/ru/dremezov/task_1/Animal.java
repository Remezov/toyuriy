package ru.dremezov.task_1;

public interface Animal {

    int getId();
    String getNickname();
    void setNickname(String nickname);
    Person getOwner();
    void setOwner(Person owner);
    double getWeight();
    void setWeight(double weight);
    String toString();
    void vaccination();
}
